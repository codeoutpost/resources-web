# vue
[vue](https://vuejs.org/)


# Front-end Resources
[bebel](http://babeljs.io/)

[2017-08-16 set babel to support ES6](www.babeljs.io/docs/plugins/transform-runtime/)

[what is webpack](https://www.ag-grid.com/ag-grid-understanding-webpack/)

# backend
[laravel](https://laravel.com/)

# python

# interested sites
[codeacademy](www.codeacademy.com)

[dropbox](www.dropbox.com)

[codeschool](www.codeschool.com)

[hackerrank](www.hackerrank.com)

[coursera](www.coursera.com)

# social network
[wordpress](www.wordpress.com)

[facebook](www.facebook.com/codeoutpost)

